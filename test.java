public class test {
    public static void main (String [] args) {

        Student a = new Student("Muhammad Afif Helmi","A17CS0101",23);
        Student b = new Student("Muhammad Hilman","A17CS0119",23);
        Student c = new Student("Mohammad Helmi","A17CS0088",23);
        Student d = new Student("Mohammad Naim","A17CS0092",23);

        AbsentStudent e = new AbsentStudent("Muhammad Faiz","A17CS0111", 23, "Overslept");
        AbsentStudent f = new AbsentStudent("Muhammad Karim","A17CS0111", 23);

        System.out.println(a.getStuName() + " " + a.getStuID() + " " + a.getStuAge());
        System.out.println(b.getStuName() + " " + b.getStuID() + " " + b.getStuAge());
        System.out.println(c.getStuName() + " " + c.getStuID() + " " + c.getStuAge());
        System.out.println(d.getStuName() + " " + d.getStuID() + " " + d.getStuAge());
        System.out.println(e.getStuName() + " " + e.getStuID() + " " + e.getStuAge() + " " + e.getReason());
        System.out.println(f.getStuName() + " " + f.getStuID() + " " + f.getStuAge());
    }
}

class Student{
    private String StuName;
    private String StuID;
    private int StuAge;

    Student(String n, String id, int  a){
        this.setStuName(n);
        this.setStuID(id);
        this.setStuAge(a);
    }

    public int getStuAge() {
        return StuAge;
    }

    public void setStuAge(int stuAge) {
        this.StuAge = stuAge;
    }

    public String getStuID() {
        return StuID;
    }

    public void setStuID(String stuID) {
        this.StuID = stuID;
    }

    public String getStuName() {
        return StuName;
    }

    public void setStuName(String stuName) {
        this.StuName = stuName;
    }
}

class AbsentStudent extends Student{
    private String reason;

    public AbsentStudent(String n, String id, int a) {
        super(n, id, a);
    }

    public AbsentStudent(String n, String id, int a, String r) {
        super(n, id, a);
        this.reason = r;
    }

    public String getReason() {
        return reason;
    }
 }